<?php

if (!function_exists('errorMessage')) {
    function errorMessage($errors, string $field)
    {
        if ($errors->has($field)) {
            return ' <div id="val-error' . $field . '" class="invalid-feedback animated fadeIn">' . $errors->first($field) . '</div>';
        }
        return null;
    }
}

if (!function_exists('errorClass')) {
    function errorClass($errors, string $field)
    {
        if ($errors->has($field)) {
            return "is-invalid";
        }
        return null;
    }
}
