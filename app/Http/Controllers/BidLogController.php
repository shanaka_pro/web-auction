<?php

namespace App\Http\Controllers;

use App\Http\Requests\BidLog\CreateRequest;
use Illuminate\Http\Request;
use App\Models\BidLog;
use App\Repositories\ItemRepository;
use App\Repositories\BidLogRepository;
use App\Repositories\AutoBidRepository;
// use Illuminate\Support\Facades\Validator;
use Yajra\DataTables\DataTables;
use Exception;
// use Illuminate\Database\Eloquent\ModelNotFoundException;
use Illuminate\Support\Facades\Auth;

class BidLogController extends Controller
{

    private $item, $bid, $autoBid;

    public function __construct()
    {
        $this->item = new ItemRepository;
        $this->bid = new BidLogRepository;
        $this->autoBid = new AutoBidRepository;
    }



    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {


        if ($request->ajax()) {

            // dd($request);


            $model = BidLog::where('item_id', Request('id'))->get();


            return Datatables::of($model)
                ->editColumn('user_id', function ($model) {

                    return $model->user->name ?? null;
                })
                ->editColumn('created_at', function ($model) {

                    return $model->created_at->format('Y-m-d H:s');
                })
                ->editColumn('is_auto_bid', function ($model) {

                    return $model->is_auto_bid == true ? "TRUE" : "FALSE";
                })
                ->editColumn('amount', function ($model) {

                    return "$".number_format($model->amount,2);
                })
                // ->addColumn('action', function($row){

                //        $btn = '<a href="test" class="edit btn btn-primary btn-sm">View</a>';

                //         return $btn;
                // })
                // ->rawColumns(['action'])
                ->make(true);
        }

        return view('bid_logs.index');
    }



    public function store(CreateRequest $request)
    {


        try {


            $itemId =   Request('item_id');
            $userId =   Auth::id();
            $amount =   Request('amount');

            $request->request->add(['user_id' => $userId]); //add request


            //check whether bidding start or not
            $this->item->validateBiddingIsStarted($userId);


            //check Latest bid and user
            $this->bid->getLatestBid($itemId, $userId, $amount);


            // dd('test');
            // throw new ModelNotFoundException('User not found by ID ');
            $params = $request->all();


            $bid = BidLog::create($params);


            return redirect()->route('bid-logs.index')->with('success', 'Record created successfully');
        } catch (Exception $e) {

            
            return redirect()->back()->with('error', $e->getMessage());


            // return redirect()->route('admin.applicants.index')->withFlashSuccess('Record updated successfully');

            // dd($e->getMessage());
            //code to handle the exception
        }


        // return redirect('bid-logs.index')->with('status', 'Profile updated!');

        // dd($bid);

    }
}
