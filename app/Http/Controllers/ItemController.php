<?php

namespace App\Http\Controllers;

use App\Http\Controllers\Controller;
use App\Models\Item;
use App\Models\BidLog;

use App\Repositories\BidLogRepository;
use App\Repositories\ItemRepository;


use Illuminate\Support\Facades\Auth;

class ItemController extends Controller
{
    private $bidLog, $item;

    public function __construct()
    {
        $this->bidLog = new BidLogRepository;
        $this->item = new ItemRepository;

    }


    public function index()
    {

        return view('items.index',[
            'items' => Item::auctionEndTime()->paginate(10)
        ]
    );

        // $category = $this->category->getModel()->where(['slug' => $category])->first();
        // $highest_price_val = $category->products()->max('price') ?? 0;
        // // $max_range_val =
        // if ($category) {
        //     session(['category' => $category]);
        //     return view('items.index',[
        //             'products' => Item::all()->paginate(8),
        //             'category' => $category,
        //             'subCategories' => $this->product->getCategories($category->id),
        //             'brands' => $this->product->getBrands($category->id),
        //             'highest_price_val' => $highest_price_val + 1000,
        //         ]
        //     );
        // }
        // return abort(404);
    }


    public function show(Item $item)
    {

        if (isset($item->auction_end_time) && $item->auction_end_time > now() ) {


            return view('items.inner.index',[
                    'item' => $item,
                    'bids' =>BidLog::orderBy('id','DESC')->paginate(10)
                ]
            );
        }
        return abort(404);
    }


}
