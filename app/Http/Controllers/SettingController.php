<?php

namespace App\Http\Controllers;

use App\Http\Controllers\Controller;

use App\Models\Setting;
use App\Http\Requests\Setting\UpdateRequest;
use Illuminate\Support\Facades\Auth;
use Exception;

use Illuminate\Http\Request;


class SettingController extends Controller
{



    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function create(Setting $setting)
    {
        // $model = $this->color->show($id);
        return view('settings.edit',compact('setting'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function store(UpdateRequest $request)
    {


        // dd($request);
        // try {


           $tt =  Setting::createOrUpdate(
            // ['user_id' =>Auth::user()->id],
            [
                'user_id'   => Auth::user()->id,
                'max_bid_amount'  => $request->get('max_bid_amount')
            ]);

            dd($tt);


        //     return redirect()->back()->with->with('success', 'Record created \ updated successfully');
        // } catch (Exception $e) {


        //     return redirect()->back()->with('error', $e->getMessage());

        // }

    }




}
