<?php

namespace App\Http\Requests\BidLog;

use Illuminate\Foundation\Http\FormRequest;
use App\Rules\CheckBidAmount;

class CreateRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }


    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {

        return [
            'amount' =>  [
                'required',
                new CheckBidAmount(),
            ],
            // 'amount' => 'required|integer|min:0', new CheckBidAmount(),
            // 'user_id' => 'required|integer|min:1',
            'item_id' => 'required|integer|min:1'
        ];
    }
}
