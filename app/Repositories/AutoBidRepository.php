<?php

namespace App\Repositories;

use App\Models\AutoBid;
use App\Models\BidLog;
use App\Models\Setting;
use App\Repositories\Repository;
use Exception;
use Illuminate\Support\Facades\DB;

class AutoBidRepository extends Repository
{

    public function __construct()
    {
        $this->model = new AutoBid();
        $this->bid = new BidLog();
        $this->setting = new Setting();

    }



    public function saveAutoBid($itemId = null,  $userId = null)
    {

        // check first autobid
        $firstAutoBid = $this->setAutoBid($itemId, $userId, false);

        $startAutoBid = $this->setAutoBid($itemId, null, true);

        return true;

    }



    public function setAutoBid($itemId = null,  $userId = null, $isAutoBid = false)
    {

        $getAutoBiddingItems = $this->getAutoBiddingItems($itemId, $userId);

        $result = [];
        foreach ($getAutoBiddingItems as $autoBiddingItems) {

            $result[] = $this->startAutoBid($autoBiddingItems->id, $autoBiddingItems->user_id, 0);

        }

        if(!empty($result)){

            return true;

        }else{

            return false;

        }


    }


    protected function createBid($lastBid, $userId, $itemId)
    {

        $data['user_id'] = $userId;
        $data['item_id'] = $itemId;
        $data['amount'] = $lastBid + 1;
        $data['is_auto_bid'] = true;

         $result = $this->bid::create($data);

         if($result){

             return true;
         }else{
            return false;
         }
    //   dd($t);
    }

    public function getAutoBiddingItems($itemId = null, $userId = null, $isAutoBid = 1)
    {

        $result = $this->model::select('items.id', 'items.auction_end_time', 'auto_bids.user_id')
            ->join('items', 'auto_bids.item_id', '=', 'items.id')
            ->where(function ($q) use ($itemId, $userId, $isAutoBid ) {

                $q->where("items.auction_end_time",">",now())
                 ->where("auto_bids.user_id","!=",$userId);


                if ($isAutoBid) {
                    $q->where("auto_bids.is_auto_bid", $isAutoBid);
                }

                if (isset($itemId)) {

                    $q->where("items.id", $itemId);
                }

                if (isset($userId)) {
                    $q->where("auto_bids.user_id", "!=", $userId);
                }

            })
            // ->groupBy("items.id")
            ->orderBy("items.id", "ASC")
            ->get();

            // dd('test');

        return $result;
    }



    protected function getLastBid($itemId)
    {
        return $this->bid::where('item_id', '=', $itemId)
            ->orderBy('id', 'DESC')
            ->first()->amount;
    }



    public function getCurrentMaxBidItem($itemId)
    {
        return $this->bid::where('item_id', $itemId)
            ->orderBy('amount', 'DESC')
            ->first()->amount;
    }


    public function startAutoBid($itemId = null, $userId = null)
    {


       $availbleBidAmount =  $this->getAvailbleBidAmount($itemId,$userId);
       $currentMaxBidItem =  $this->getCurrentMaxBidItem($itemId);

       $lastBid = $this->getLastBid($itemId);

       if(($availbleBidAmount > $currentMaxBidItem ) && ($availbleBidAmount > $lastBid)){

        return $this->createBid($lastBid, $userId, $itemId);

       }else{

           return false;
       }


    //    return  ($availbleBidAmount > $currentMaxBidItem ) && ($availbleBidAmount > $lastBid) ? true : false;

    }

    public function getAvailbleBidAmount($itemId = null, $userId = null)
    {

        // $this->bid->items->sum('price');

        $highestBidAmounts = $this->bid::select('bid_logs.amount')
        ->join('settings', 'bid_logs.item_id', '=', 'settings.id')
        ->where(function ($q) use ($itemId, $userId ) {

            $q->where("bid_logs.user_id",$userId)
             ->where("bid_logs.is_auto_bid",1)
             ->where("bid_logs.item_id","!=", $itemId);

        })
        ->orderBy("bid_logs.amount", "DESC")
        // ->distinct()
        ->get();

        $sumHighestBidAmount = 0;

        foreach($highestBidAmounts as $highestBidAmount){

            $sumHighestBidAmount += $highestBidAmount->amount;

        }


        $setting = $this->setting::where('user_id', $userId)
        ->orderBy('settings.id','desc')
        ->first('max_bid_amount');

        $settingBidAmount = $setting->max_bid_amount ?? 0;

        $result = $settingBidAmount - $sumHighestBidAmount;

    return  $result;

    }
}
