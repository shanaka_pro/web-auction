<?php

namespace App\Repositories;

use App\Models\BidLog;
use App\Repositories\Repository;
use Exception;


class BidLogRepository extends Repository
{

    public function __construct()
    {
        $this->model = new BidLog();
    }


    public function getLatestBid($itemId, $userId, $amount) {


        $latestBid =$this->model::where('item_id', '=', $itemId)
            ->orderBy('amount', 'DESC')
            ->first();

            if ($amount <= $latestBid->amount) {

                throw new Exception('Bid amount should be greater than (USD)'.number_format($latestBid->amount,2));
                
            } else if ($userId == $latestBid->user_id) {

                throw new Exception('Bidded highest amount already');
            }

    }





}
