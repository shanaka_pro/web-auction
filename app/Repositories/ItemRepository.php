<?php

namespace App\Repositories;

use App\Models\Item;
use App\Repositories\Repository;
use Exception;

class ItemRepository extends Repository
{


    public function __construct()
    {
        $this->model = new Item();
    }


    public function validateBiddingIsStarted($itemId) {

        $item = $this->model::find($itemId);

        if (is_null($item->auction_end_time)) {

            throw new Exception('Auction is not started');

        } else if ($item->auction_end_time < NOW() ) {

            throw new Exception('Auction closed');
        }
    }



    // protected function validateAutoBiddingNotEnabled($uerId, $itemId) {
    //     $autoBidStatus = AutoBidStatus::firstOrCreate(['user_id'=>$uerId, 'item_id'=>$itemId], []);
    //     if ($autoBidStatus->auto_bid_enabled) {
    //         throw new BadRequestException('Manual bidding is not allowed when auto bidding is enabled');
    //     }
    // }



}
