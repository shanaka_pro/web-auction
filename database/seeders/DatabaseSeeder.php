<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\Schema;

class DatabaseSeeder extends Seeder
{
    /**
     * Seed the application's database.
     *
     * @return void
     */
    public function run()
    {

        Schema::disableForeignKeyConstraints();
        \App\Models\User::factory(1)->create();

        \App\Models\AutoBid::truncate();
        \App\Models\AutoBid::factory(2)->create();

        \App\Models\Item::truncate();
        \App\Models\Item::factory(20)->create();

        \App\Models\BidLog::truncate();
        \App\Models\BidLog::factory(20)->create();


        $this->call(UsersTableSeeder::class);
    }
}
