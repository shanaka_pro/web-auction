<?php
namespace Database\Seeders;

use Illuminate\Support\Facades\DB;
use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\Hash;

class UsersTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('users')->insert([
            'name' => 'user1',
            'email' => 'user1@gmail.com',
            'email_verified_at' => now(),
            'password' => Hash::make('user1'),
            'created_at' => now(),
            'updated_at' => now()
        ],
        [
            'name' => 'user2',
            'email' => 'user2@gmail.com',
            'email_verified_at' => now(),
            'password' => Hash::make('user2'),
            'created_at' => now(),
            'updated_at' => now()
        ]
    );
    }
}
