<aside class="aside">
    <div class="wrap-aside">
       <div class="brand-filter">
          <h5 class="filter-title">Brands</h5>
          <ul id="filter-brands" class="filter-options">
                <li>
                    <label for="all">
                    <div>
                        <input type="checkbox" name="brand" class="checkbox" id="all" value="filter_all" data-filter_id="all">
                        <span class="check-icon"></span>
                    </div>
                    <div>
                        <h4>All</h4>
                    </div>
                    </label>
                </li>
             {{--  @foreach ($brands as $brand)
                <li>
                    <label for="brand-{{ $brand->id }}">
                    <div>
                        <input checked="checked" type="checkbox" name="brand" class="checkbox" id="brand-{{ $brand->id }}" value="{{ $brand->id }}" data-filter_id="media">
                        <span class="check-icon"></span>
                    </div>
                    <div>
                        <h4>{{ $brand->name }}</h4>
                    </div>
                    </label>
                </li>
             @endforeach  --}}
          </ul>
       </div>
       <div class="brand-filter">
          <h5 class="filter-title">Categories</h5>
          <ul id="filter-brands" class="filter-options">
             <li>
                <label for="catAll">
                   <div>
                      <input type="checkbox" name="sub-category"   class="checkbox" id="catAll" value="filter_all" data-filter_id="all">
                      <span class="check-icon"></span>
                   </div>
                   <div>
                      <h4>All</h4>
                   </div>
                </label>
             </li>
             {{--  @foreach ($subCategories as $subCategory)
                <li>
                    <label for="sub-category-{{ $subCategory->id }}">
                        <div>
                            <input checked="checked" name="sub-category" class="checkbox" type="checkbox" value="{{ $subCategory->id }}" data-filter_id="red" id="sub-category-{{ $subCategory->id }}">
                            <span class="check-icon"></span>
                        </div>
                        <div>
                            <h4>{{ $subCategory->name }}</h4>
                        </div>
                    </label>
                </li>
             @endforeach  --}}
          </ul>
       </div>
       <div class="brand-filter">
          <h5 class="filter-title">Price Range(LKR)</h5>
          <div class="range-side">
             <input 0="0" 1="1" 100="100" type="range" min="min" default="default" max="{{ $highest_price_val ?? '' }}" step="1" value="0" +="+" data-orientation="horizontal" horizontal="horizontal">
             <h3>
                <span>0 to
                </span>
                <output>
                0</output>
             </h3>
          </div>
       </div>
       <div class="clear-filter">
          <button class="btn">Clear Filters</button>
       </div>
    {{--  @if (isset($category) && $category->slug == 'air-conditioners')
       <div class="hr"></div>
       <div class="btu-calculator">
          <h5 class="filter-title">BTU Calculator</h5>
       </div>
    </div>
    <div class="btu-cal">
       <div class="cal-sec">
          <div class="wrap-input-iteam">
             <p class="cla-title">Size of the area</p>
             <div class="cal-row">
                <div class="left-selec">
                   <input type="text" class="form-control cal-input" placeholder="100">
                </div>
                <div class="right-select">
                   <select class="form-control frm-right frm-select">
                      <option value="cat-red">sqft</option>
                      <option value="cat-all">sqft</option>
                      <option value="cat-all">sqft</option>
                   </select>
                </div>
             </div>
          </div>
          <div class="wrap-input-iteam">
             <p class="cla-title">Room Ceiling Height</p>
             <div class="cal-row">
                <div class="left-selec">
                   <input type="text" class="form-control cal-input" placeholder="2.5">
                </div>
                <div class="right-select">
                   <select class="form-control frm-right frm-select">
                      <option value="cat-red">m</option>
                      <option value="cat-all">m</option>
                      <option value="cat-all">m</option>
                   </select>
                </div>
             </div>
          </div>
          <div class="wrap-input-iteam">
             <p class="cla-title">No of people inside regularly</p>
             <div class="cal-row">
                <div class="full-sec">
                   <input type="text" class="form-control cal-input" placeholder="4">
                </div>
             </div>
          </div>
          <div class="wrap-input-iteam">
             <p class="cla-title">Type of the Area</p>
             <div class="cal-row">
                <div class="full-sec">
                   <select class="form-control frm-right frm-select">
                      <option value="cat-red">Living Room</option>
                      <option value="cat-all">Living Room</option>
                      <option value="cat-all">Living Room</option>
                   </select>
                </div>
             </div>
          </div>
          <div class="wrap-input-iteam">
             <p class="cla-title">Insulation Condition</p>
             <div class="cal-row">
                <div class="full-sec">
                   <select class="form-control frm-right frm-select">
                      <option value="cat-red">Average</option>
                      <option value="cat-all">Average</option>
                      <option value="cat-all">Average</option>
                   </select>
                </div>
             </div>
          </div>
          <div class="wrap-input-iteam">
             <p class="cla-title">Sun Exposure</p>
             <div class="cal-row">
                <div class="full-sec">
                   <select class="form-control frm-right frm-select">
                      <option value="cat-red">Average</option>
                      <option value="cat-all">Average</option>
                      <option value="cat-all">Average</option>
                   </select>
                </div>
             </div>
          </div>
          <div class="wrap-input-iteam">
             <p class="cla-title">Climate</p>
             <div class="cal-row">
                <div class="full-sec">
                   <select class="form-control frm-right frm-select">
                      <option value="cat-red">Average</option>
                      <option value="cat-all">Average</option>
                      <option value="cat-all">Average</option>
                   </select>
                </div>
             </div>
          </div>
          <div class="wrap-input-iteam">
             <button type="submit" class="calculate">CALCULATE</button>
          </div>
       </div>
       <div class="cal-result">
          <ul>
             <li>18,000<span>BTU</span></li>
             <li>5200<span>Watts</span></li>
             <li>1.6<span>Ton</span></li>
          </ul>
       </div>
    </div>
     @endif()  --}}
 </aside>
