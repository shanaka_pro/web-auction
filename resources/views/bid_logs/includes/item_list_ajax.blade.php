<section class="filter-sec-producta">
    <div class="row">
        <div class="col-xl-12">
            <div class="grid-box grid-container">
                <div class="row" id="product-list">
                    @if ($productsAjax->count() > 0)

                        @foreach ($productsAjax as $product)
                            <div class="col-xl-3 col-lg-3 col-md-6 col-sm-12">
                                <a href="{{ route('fr.product.inner', ['slug' => $product->slug]) }}">
                                    <div class="prod-iteam">
                                        <div class="prod-image-cov">
                                            <img src="{{ $product->images }}" alt="{{ $product->slug }}"
                                                class="prod-img">
                                        </div>

                                        <div class="prod-detail-section">
                                            <div class="prod-details">
                                                <h6 class="prod-detail">{{ $product->name }}</h6>
                                                <span class="prod-code">{{ $product->model }}</span>
                                            </div>
                                            <div class="cart-hr">
                                                <div class="prod-value">
                                                    <p class="price">Rs.
                                                        {{ number_format($product->discounted_price, 2) }}</p>

                                                    @if ($product->discount > 0)
                                                        <p>
                                                            <span>Rs.
                                                                {{ number_format($product->price, 2) }}</span>({{ $product->discount }}%
                                                            off)
                                                        </p>
                                                    @endif

                                                </div>
                                                <div class="btn btn-cart">
                                                    <img src="/images/cart.svg">
                                                </div>
                                            </div>

                                            <a href="{{ route('fr.product.compare',$product->id) }}" class="compare">
                                                <img src="/images/compare.svg">
                                                <span class="text-uppercase">COMPARE</span>
                                            </a>

                                        </div>
                                    </div>
                                </a>
                            </div>
                        @endforeach

                        <div >
                            {!! $productsAjax->links() !!}
                        </div>
                    @endif

                </div>

            </div>

        </div>

        {{-- <div style="text-align:center; box-sizing: border-box; display:block"> --}}
        {{-- <div class="grid-box grid-container"> --}}

    </div>
</section>
