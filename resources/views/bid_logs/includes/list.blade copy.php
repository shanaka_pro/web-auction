<section class="filter-sec-producta">
    <div class="row">
        <div class="col-xl-12">
            <div class="grid-box grid-container">
                <div class="row" id="product-list">
                    @if ($bidLogs->count() > 0)


                    <table class="with-radius left-align default-font">
                        <thead>
                            <tr>
                                <th class="skip blue"></th>
                                <th class="head" style="text-align:right">
                                    <p>Item name</p>
                                </th>
                                <th class="head" style="text-align:right">
                                    <p>Item name</p>
                                </th>
                                <th class="head" style="text-align:right">
                                    <p>Bid Amount (USD)</p>
                                </th>
                            </tr>
                        </thead>
                        <tbody>
                            @foreach ($bidLogs as $bidLog)

                            <tr>
                                <td> {{ $bidLog->id ?? null }}</td>
                                <td> test test </td>
                                <td> test test </td>
                                <td style="text-align:right">{{ number_format($bidLog->amount,2) ?? null }}</td>

                            </tr>

                            @endforeach

                        </tbody>
                    </table>



                    @endif

                </div>

            </div>

        </div>
    </div>

    <div class="row">
        <div class="col-md-12">
            <div class="filter-pagination">
                {!! $bidLogs->links() !!}
            </div>
        </div>
    </div>
</section>
