@extends('layouts.app',['page'=>'Products','body_class'=>'products'])

@section('content')

{{--  @include('items.includes.header')  --}}

<div class="item-section">
    <div class="container">
        <div class="row">
            <div class="col-xl-12">
                <div class="wrap-item-section">

                    {{--  @include('items.includes.filter')  --}}

                    <div id="wrap-item-section">

                        @include('bid_logs.includes.list')

                    </div>

                </div>
            </div>
        </div>
    </div>
</div>
@endsection
@push('scripts')

<script type="text/javascript">
    $.ajaxSetup({
            headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            }
        });

        //define route
        var pathFilterProducts = "";
        var categoryId = "{{ $products[0]->category_id ?? 0}}";

        $(document).ready(function() {

            $('input[value="filter_all"]').click(function() {
                $("input[name="+$(this).attr('name')+"]").prop("checked", $(this).prop("checked"));
            });



            $('input[type="checkbox"]').on('change', function(event) {
                event.preventDefault();
                if($(this).val() != 'filter_all'){
                    $("input[name="+$(this).attr('name')+"][value='filter_all']").prop("checked", false);
                }


                var subCategories = [];
                var brands = [];

                $.each($("input[name='sub-category']:checked"), function() {
                    subCategories.push($(this).val());
                });

                $.each($("input[name='brand']:checked"), function() {
                    brands.push($(this).val());
                });

                $.post("", {
                    categoryId: categoryId,
                    brands: brands,
                    subCategories: subCategories
                }, function(result) {

                    $('#wrap-item-section').empty().append(result.data);
                });

            });


            var output = document.querySelectorAll('output')[0];
            $('input[type=range]').rangeslider({
                polyfill: false,
                onSlideEnd: function(position, price) {
                    $.post(pathFilterProducts, {
                    categoryId: categoryId,
                    price: output.innerHTML

                    },
                    function(result) {
                        $('#wrap-item-section').empty().append(result.data);
                    });
                    console.log( price)
                }
            });






            $('#newest-first').on('click', function(event) {
                event.preventDefault();
                $.post(pathFilterProducts, {
                    categoryId: categoryId,
                    newestFirst: true,
                }, function(result) {
                    $('#wrap-item-section').empty().append(result.data);
                });
            });


            $('#price-diff').on('change', function(event) {
                event.preventDefault();
                $.post(pathFilterProducts, {
                    categoryId: categoryId,
                    priceDiff: $(this).val()
                }, function(result) {
                    $('#wrap-item-section').empty().append(result.data);
                });
            });

        });

</script>
@endpush
