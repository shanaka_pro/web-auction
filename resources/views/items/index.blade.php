@extends('layouts.app', ['activePage' => 'table', 'titlePage' => __('Table List')])

@section('content')
  <div class="content">
    <div class="container-fluid">
      <div class="row">
        <div class="col-md-12">
        @include('items.includes.list')
        </div>
      </div>
    </div>
  </div>
@endsection



