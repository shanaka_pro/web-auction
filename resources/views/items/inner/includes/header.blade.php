<section class="product-page-hader">
    <div class="container">

        <div class="wrap-product-page-hader">
            <h5>{{ $item->name }}</h5>
            <div class="layout-filter">
                <a href="{{ route('items.index') }}" class="back-btn">

                    <span>Back</span>
                </a>
            </div>
        </div>
    </div>
</section>
