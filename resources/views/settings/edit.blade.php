@extends('layouts.app', ['activePage' => 'settings', 'titlePage' => __('Settings')])

@section('content')
  <div class="content">
    <div class="container-fluid">
      <div class="row">
        <div class="col-md-12">

          <form method="post" action="{{ route('settings.store') }}" autocomplete="off" class="form-horizontal">
            @csrf
            @method('post')

            <div class="card ">
              <div class="card-header card-header-primary">
                <h4 class="card-title">{{ __('Edit Settings') }}</h4>
                <p class="card-category">{{ __('Maximum bid Amount') }}</p>
              </div>


              <div class="card-body ">
                @if (session('status'))
                  <div class="row">
                    <div class="col-sm-12">
                      <div class="alert alert-success">
                        <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                          <i class="material-icons">close</i>
                        </button>
                        <span>{{ session('status') }}</span>
                      </div>
                    </div>
                  </div>
                @endif

                <div class="row">
                  <label class="col-sm-2 col-form-label">{{ __('Maximum Bid Amount') }}</label>
                  <div class="col-sm-7">
                    <div class="form-group{{ $errors->has('max_bid_amount') ? ' has-danger' : '' }}">
                      <input class="form-control{{ $errors->has('max_bid_amount') ? ' is-invalid' : '' }}" name="max_bid_amount" id="input-name" type="text" placeholder="{{ __('Name') }}" value="" required="true" aria-required="true"/>
                      @if ($errors->has('max_bid_amount'))
                        <span id="name-error" class="error text-danger" for="input-name">{{ $errors->first('max_bid_amount') }}</span>
                      @endif
                    </div>
                  </div>
                </div>


              </div>
              <div class="card-footer ml-auto mr-auto">
                <button type="submit" class="btn btn-primary">{{ __('Save') }}</button>
              </div>
            </div>
          </form>
        </div>
      </div>



    </div>
  </div>
@endsection
