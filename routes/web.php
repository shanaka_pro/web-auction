<?php

use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Auth::routes();

// Route::post('login', 'LoginController@login');
// Route::get('/login', [App\Http\Controllers\Auth\LoginController::class, 'login'])->name('login');
// Route::get('/logout', [App\Http\Controllers\Auth\LoginController::class, 'logout'])->name('logout');
// Route::post('/logout', 'AuthController@logout');

// Route::get('/home', [App\Http\Controllers\HomeController::class, 'index'])->name('home');

Route::group(['middleware' => 'auth'], function() {

Route::get('/items', [App\Http\Controllers\ItemController::class, 'index'])->name('items.index');

Route::get('/items/{item}', [App\Http\Controllers\ItemController::class, 'show'])->name('items.show');

Route::post('/bids-logs', [App\Http\Controllers\BidLogController::class, 'store'])->name('bid-logs.store');

Route::get('/bid-logs', [App\Http\Controllers\BidLogController::class, 'index'])->name('bid-logs.index');
Route::get('/bid-logs', [App\Http\Controllers\BidLogController::class, 'index'])->name('bid-logs.index');

// Route::get('/settings/{settings}', [App\Http\Controllers\SettingController::class, 'edit'])->name('settings.edit');

// Route::put('/settings/{settings}', [App\Http\Controllers\SettingController::class, 'update'])->name('settings.update');

Route::resource('/settings', App\Http\Controllers\SettingController::class)->only(['create', 'store']);
});


Route::get('/', function () {
    return view('welcome');
});

Auth::routes();

Route::get('/home', [App\Http\Controllers\HomeController::class, 'index'])->name('home');
Auth::routes();

Route::get('/home', 'App\Http\Controllers\HomeController@index')->name('home')->middleware('auth');

Route::group(['middleware' => 'auth'], function () {
	Route::get('table-list', function () {
		return view('pages.table_list');
	})->name('table');

	Route::get('typography', function () {
		return view('pages.typography');
	})->name('typography');

	Route::get('icons', function () {
		return view('pages.icons');
	})->name('icons');

	Route::get('map', function () {
		return view('pages.map');
	})->name('map');

	Route::get('notifications', function () {
		return view('pages.notifications');
	})->name('notifications');

	Route::get('rtl-support', function () {
		return view('pages.language');
	})->name('language');

	Route::get('upgrade', function () {
		return view('pages.upgrade');
	})->name('upgrade');
});

Route::group(['middleware' => 'auth'], function () {
	Route::resource('user', 'App\Http\Controllers\UserController', ['except' => ['show']]);
	Route::get('profile', ['as' => 'profile.edit', 'uses' => 'App\Http\Controllers\ProfileController@edit']);
	Route::put('profile', ['as' => 'profile.update', 'uses' => 'App\Http\Controllers\ProfileController@update']);
	Route::put('profile/password', ['as' => 'profile.password', 'uses' => 'App\Http\Controllers\ProfileController@password']);
});

